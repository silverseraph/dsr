CC = valac
PKGS = --pkg gio-2.0 --pkg posix
CFLAGS = -X -O3
SRC = main.vala
OUT = dsr

.PHONY: make debug

make:
	$(CC) $(SRC) $(CFLAGS) $(PKGS) -o $(OUT)

debug:
	$(CC) $(SRC) $(CFLAGS) $(PKGS) -o $(OUT) -D DEBUG 
