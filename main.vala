extern void exit(int exit_code);

int main(string[] args)
{
    string rex_term = "";
    bool rex_set = false;
    uint64 max_depth = 20;
    bool list_directories = true;
    bool show_extensions = false;
    bool color = true;
    bool do_inline = false;

    RegexCompileFlags rcf = 0;

    for(int i = 1; i < args.length; i++)
    {
        switch(args[i])
        {
            case "-h":
            case "--help":
                show_help(args[0]);
                exit(0);
                break;
            case "-m":
            case "--max-depth":
                i++;
                if(i < args.length)
                {
                    if(!uint64.try_parse(args[i], out max_depth)) max_depth = 5;
                }
                break;
            case "-c":
            case "--color":
                color = true;
                break;
            case "-C":
            case "--no-color":
                color = false;
                break;
            case "-d":
            case "--list-directories":
                list_directories = true;
                break;
            case "-D":
            case "--hide-directories":
                list_directories = false;
                break;
            case "-e":
            case "--show-type":
                show_extensions = true;
                break;
            case "-E":
            case "--hide-type":
                show_extensions = false;
                break;
            case "-i":
            case "--ignore-case":
                rcf = RegexCompileFlags.CASELESS;
                break;
            case "-I":
            case "--no-ignore-case":
                rcf = 0;
                break;
            case "-l":
                do_inline = true;
                break;
            case "-L":
                do_inline = false;
                break;
            default:
                if(!rex_set)
                {
                    rex_term = args[i];
                    rex_set = true;
                }
                break;
        }
    }

    if(Posix.isatty(stdout.fileno()))
    {
        //color &= can_use_color();
        color &= true;
    }
    else
    {
#if DEBUG
        stderr.printf("Not outputting to a tty.  Turning off color.\n");
#endif
        color = false;
    }

#if DEBUG
    stdout.printf("DEBUG: Settings:\n");
    stdout.printf("DEBUG: color            = %s\n", color ? "true" : "false");
    stdout.printf("DEBUG: list_directories = %s\n", list_directories ? "true" : "false");
    stdout.printf("DEBUG: show_extensions  = %s\n", show_extensions ? "true" : "false");
    stdout.printf("DEBUG: rex_set          = %s\n", rex_set ? "true" : "false");
    stdout.printf("DEBUG: max_depth        = %llu\n", max_depth);
    stdout.printf("DEBUG: rex_term         = %s\n", rex_term);
#endif

    Regex rex = null;

    try
    {
        rex = new Regex(rex_term, (RegexCompileFlags.OPTIMIZE | rcf));
    }
    catch(Error e)
    {
        stderr.printf("Error: %s\n", e.message);
        exit(1);
    }

    File cf;
    FileInfo file_info;
    FileEnumerator enumerator;
    MatchInfo match_info;
    Queue<Element> search_queue = new Queue<Element>();

    Element el = new Element(0, ".");
    search_queue.push_tail(el);

    int lgi = 0;
    int match_start = 0;
    int match_end   = 0;

    string f_name;
    const string color_red = "\033[31m";
    const string color_default = "\033[39m";

    while((el = search_queue.pop_tail()) != null)
    {
        try
        {
            #if DEBUG
            stdout.printf("DEBUG: Now expanding %s\n", el.name);
            #endif

            cf = File.new_for_path(el.name);

            enumerator = cf.enumerate_children("standard::name,standard::type", 0);

            while((file_info = enumerator.next_file()) != null)
            {
                f_name = el.name + "/" + file_info.get_name();

                var ft = file_info.get_file_type();

                if((ft == FileType.DIRECTORY))
                {
                    if(el.level < max_depth)
                    {
                        #if DEBUG
                        stdout.printf("DEBUG: Now adding %s to the queue at level %d.\n", f_name, (el.level + 1));
                        #endif
                        search_queue.push_tail(new Element(el.level + 1, f_name));
                    }
                    else
                    {
                        #if DEBUG
                        stdout.printf("Will not add %s to the search queue, because it is too deep.\n", f_name);
                        #endif
                    }
                }
                else
                {
                    #if DEBUG
                    stdout.printf("DEBUG: %s is not a directory\n", f_name);
                    #endif
                }

                if(show_extensions)
                {
                    switch(ft)
                    {
                        case FileType.DIRECTORY:
                            #if DEBUG
                            stdout.printf("DEBUG: File type of %s is: directory\n", f_name);
                            #endif
                            f_name += "/";
                            break;
                        case FileType.SYMBOLIC_LINK:
                            #if DEBUG
                            stdout.printf("DEBUG: File type of %s is: sym_link\n", f_name);
                            #endif
                            f_name += "@";
                            break;
                        case FileType.SPECIAL:
                            #if DEBUG
                            stdout.printf("DEBUG: File type of %s is: special\n", f_name);
                            #endif
                            f_name += "#";
                            break;
                        case FileType.MOUNTABLE:
                            #if DEBUG
                            stdout.printf("DEBUG: Fiel type of %s is: mountable\n", f_name);
                            #endif
                            f_name += "&";
                            break;
                        default:
                            break;
                    }
                }

                if(
                    (ft != FileType.DIRECTORY) ||
                    (
                        (ft == FileType.DIRECTORY) &&
                        list_directories
                    )
                  )
                {
                    if(rex.match(f_name, 0, out match_info))
                    {
                        if(!color)
                        {
                            if(do_inline)
                                stdout.printf("%s ", f_name);
                            else
                                stdout.printf("\t%s\n", f_name);
                        }
                        else
                        {
                            lgi = 0;
                            match_info.fetch_pos(0, out match_start, out match_end);
                            if(!do_inline)
                                stdout.putc('\t');
                            stdout.puts(color_default);
                            stdout.puts(f_name[lgi:match_start]);
                            stdout.puts(color_red);
                            stdout.puts(f_name[match_start:match_end]);
                            lgi = match_end;
                            while(match_info.next())
                            {
                                match_info.fetch_pos(0, out match_start, out match_end);
                                stdout.puts(color_default);
                                stdout.puts(f_name[lgi:match_start]);
                                stdout.puts(color_red);
                                stdout.puts(f_name[match_start:match_end]);
                                lgi = match_end;
                            }
                            stdout.puts(color_default);
                            stdout.puts(f_name[lgi:f_name.length]);
                            if(!do_inline)
                                stdout.putc('\n');
                            else
                                stdout.putc(' ');
                        }
                    }
                }
            }

        }
        catch(Error e)
        {
            stderr.printf("Error: %s\n", e.message);
            continue;
        }
    }

    return 0;
}

void show_help(string name)
{
    stdout.printf("\tUsage: %s [regex] [-hcCdDeE] [--help] [--colors] [--no-color]\n", name);
    stdout.printf("\t                            [--list-directories] [--hide-directories]\n");
    stdout.printf("\t                            [--show-extensions] [--hide-extensions]\n");
    stdout.printf("\t                            [-m <depth>] [--max-depth <depth>]\n");
    stdout.printf("\t-h || --help                Lists help information\n");
    stdout.printf("\t-c || --colors              Sets whether to use color, if it can be used.\n");
    stdout.printf("\t-C || --no-color            Prohibits color from being displayed.  Color is displayed by default.\n");
    stdout.printf("\t-m || --max-depth <depth>   Set the max depth to search. This helps to limit the runtime.\n");
    stdout.printf("\t-d || --list-directories    Include directories in the search.\n");
    stdout.printf("\t-D || --hide-directories    Hide directories from the search.\n");
    stdout.printf("\t-e || --show-type           Show file type extensions at the end of the name.\n");
    stdout.printf("\t-E || --hide-type           Hide file type extensions at the end of the name.\n");
    stdout.printf("\t-i || --ignore-case         Ignore the case given by the regex\n");
    stdout.printf("\t-I || --no-ignore-case      Make the regex case sensitive\n");
}

public class Element
{
    public int level = 0;
    public string name = null;
    public Element(int level, string name)
    {
        this.level = level;
        this.name = name;
    }
}
